#include <LCD_I2C.h>
#include "moter.h"

struct repeating_timer st_timer; //タイマー割り込み用の構造体

#define TEST_SW1 (20)//テストSW1はGP20
#define TEST_SW2 (21)//テストSW2はGP21
#define TEST_LED1 (12)//テストLED1はGP12
#define TEST_LED2 (13)//テストLED2はGP13

LCD_I2C lcd(0x27, 16, 2);//LCD関数をインスタンス化
char lcd_buff1[17];//16文字＋エンドコードで17文字分確保(1行目)
char lcd_buff2[17];//16文字＋エンドコードで17文字分確保(2行目)

//  タイマー割り込み処理
bool Timer(struct repeating_timer *t) {
  int sen2ad=0, sen3ad=0;//センサのAD値
  float pwm_r=0, pwm_l=0, Pid_val_R=0, Pid_val_L=0;

  sen2ad = sen2_ad();//センサ2のAD変換
  sen3ad = sen3_ad();//センサ3のAD変換

  Pid_val_L = PID_LIGHT_L(sen2ad, 720);//左のPID演算
  Pid_val_R = PID_LIGHT_R(sen3ad, 720);//右のPID演算
  pwm_l = constrain( (580 + Pid_val_L - Pid_val_R), 0, 1023 );//左PWM値演算
  pwm_r = constrain( (580 + Pid_val_R - Pid_val_L), 0, 1023 );//右PWM値演算
  moter_pwm_set(L, pwm_l);//左モータPWM設定
  moter_pwm_set(R, pwm_r);//右モータPWM設定
  
  //動作確認用にテストLEDをフリッカさせる
  if( digitalRead(TEST_LED1) == HIGH ){ digitalWrite(TEST_LED1, LOW); }
  else{ digitalWrite(TEST_LED1, HIGH); }

  return true;
}

void setup() {
  // put your setup code here, to run once:
  main_wait_ini(50);//main処理の待ち時間50msにセット
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TEST_SW1, INPUT_PULLUP);//TEST_SW1をプルアップで入力モードに設定
  pinMode(TEST_SW2, INPUT_PULLUP);//TEST_SW2をプルアップで入力モードに設定
  pinMode(TEST_LED1, OUTPUT);//TEST_LED1を出力モードに設定
  pinMode(TEST_LED2, OUTPUT);//TEST_LED2を出力モードに設定
  lcd.begin();//LCD初期化
  lcd.backlight();//バックライトON
  moter_begin();//モータ関連初期化
  sensor_begin();//センサ関連初期化
  // タイマーの初期化(割込み間隔はusで指定)
  add_repeating_timer_ms(10, Timer, NULL, &st_timer);
}

void loop() {
  // put your main code here, to run repeatedly:
  static unsigned long main_wait_time;//メイン周期の待った時間（us）
  main_wait_time = main_wait();//メイン周期待ち関数
  /* この関数以降は50ms周期でループする */
  main_50ms();//50ms毎処理を呼び出す

  lcd.setCursor(0, 0);//カーソル位置を1文字目の1行目にする
  lcd.print(lcd_buff1);//1行目の文字データを表示
  lcd.setCursor(0, 1);//カーソル位置を1文字目の2行目にする
  lcd.print(lcd_buff2);//2行目の文字データを表示
}

void main_50ms(void){


}
