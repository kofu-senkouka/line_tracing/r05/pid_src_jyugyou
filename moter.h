#ifndef moter_h //多重インクルード防止
#define moter_h

#define R_CW_CCW (9)
#define L_CW_CCW (8)
#define R_PWM (2)
#define L_PWM (3)
#define R_BRK (6)
#define L_BRK (7)
#define R true
#define L false
#define BRK_ON true
#define BRK_OFF false
#define CW true
#define CCW false


#endif