#include "moter.h"

void moter_mode(bool _r_l, bool _cw_ccw){
  if( _r_l == R ){
    if( _cw_ccw == CW ){ digitalWrite(R_CW_CCW, LOW); }
    else{ digitalWrite(R_CW_CCW, HIGH); }
  }
  else{
    if( _cw_ccw == CW ){ digitalWrite(L_CW_CCW, LOW); }
    else{ digitalWrite(L_CW_CCW, HIGH); }    
  }
}

/* モータのブレーキ処理関数 */
void moter_brk( bool _r_l, bool _brk ){
  if( _r_l == R ){ 
    if( _brk == BRK_OFF ){ digitalWrite(R_BRK, LOW); }
    else{ digitalWrite(R_BRK, HIGH); }
  }
  else{
    if( _brk == BRK_OFF ){ digitalWrite(L_BRK, LOW); }
    else{ digitalWrite(L_BRK, HIGH); }
  }
}

/* PWMの設定をする関数 */
void moter_pwm_set(bool _r_l, ushort _pwm){
  ushort _pwm_set=0;//今回設定するPWMの値（処理結果）
  static ushort _r_pwm_old=0, _l_pwm_old=0;//前回のpwm設定値

  if( _r_l == R ){
    if( _pwm >= _r_pwm_old ){//指示されているpwm値が大きい
      _pwm_set = min( _pwm, (_r_pwm_old + 100) );//加速する処理
    }
    else{
      _pwm_set = max( _pwm, (_r_pwm_old - 100) );//減速する処理
    }
    analogWrite(R_PWM, _pwm_set);//PWM値を設定
    _r_pwm_old = _pwm_set;//前回の値を今回指示した値に更新
  }
  else{
    if( _pwm >= _l_pwm_old ){//指示されているpwm値が大きい
      _pwm_set = min( _pwm, (_l_pwm_old + 100) );//加速する処理
    }
    else{
      _pwm_set = max( _pwm, (_l_pwm_old - 100) );//減速する処理
    }
    analogWrite(L_PWM, _pwm_set);//PWM値を設定
    _l_pwm_old = _pwm_set;//前回の値を今回指示した値に更新
  }
}

/* モータ関連の初期化関数 */
void moter_begin(void){
  pinMode(R_CW_CCW, OUTPUT);
  pinMode(L_CW_CCW, OUTPUT);
  pinMode(R_BRK, OUTPUT);
  pinMode(L_BRK, OUTPUT);
  pinMode(R_PWM, OUTPUT);
  pinMode(L_PWM, OUTPUT);
  analogWriteFreq(500);//PWMの周波数を500Hzに設定
  analogWriteRange(1023);//PWMの分解能を1024段階に設定

  moter_mode(R, CW);//右モータ正転
  moter_mode(L, CW);//左モータ正転
  moter_brk(R, BRK_OFF);//右モータブレーキOFF
  moter_brk(L, BRK_OFF);//左モータブレーキOFF
  moter_pwm_set(R, 0);//右モータPWM０％
  moter_pwm_set(L, 0);//左モータPWM０％
}








