#define DELTA_T (0.01) //演算周期(50ms)
// #define KP (0.4) //Pゲイン
////#define KP (0.45) //Pゲイン
#define KP (0.45) //Pゲイン
// #define KI (0.025) //Iゲイン 
// #define KD (0.00625) //Dゲイン
// #define KI (0.025) //Iゲイン 周期0.3s
#define KI (0.0) //Iゲイン 周期0.3s
// #define KD (0.0375) //Dゲイン 周期0.3s
////#define KD (0.04) //Dゲイン 周期0.3s
#define KD (0.065) //Dゲイン 周期0.3s
// #define KI (0.0) //Iゲイン 周期0.5s～0.1s
// #define KD (0.0) //Dゲイン



// PID制御上下限クリップ処理 ///////////////////////////////////////
float math_limit(float pid){
  pid = constrain(pid, -350, 350); //引数を上下限クリップ
  return pid;
}

// 左モータ用PID制御 /////////////////////////////////////////////
float PID_LIGHT_L(short Senser_Value, short Target_Value){
  static long diff_L[2]={0};//偏差[0]=前回、[1]=今回
  static float integral_L=0;//積分の値（合計）
  float p=0,i=0,d=0;//今回の各値

  diff_L[0] = diff_L[1];//偏差の前回の値更新
  diff_L[1] = Target_Value - Senser_Value;//目標との偏差(今回)を計算
  p = KP * diff_L[1];//P項目を計算
  if( (diff_L[1] > 100) || (diff_L[1] < -100) ){
    //処理なし(1時停止)
  }
  else{
    integral_L += (diff_L[1] + diff_L[0]) * DELTA_T / 2;//積分がグラフの面積(台形の総計)で求める
  }
  i = KI * integral_L;//I項目を計算
  d = KD * (diff_L[1] - diff_L[0]) / DELTA_T;//p項目は偏差の差で求める
  snprintf(lcd_buff1, 16, "%3.0f, %3.0f, %3.0f", p, i, d);
  return math_limit(p+i+d);
}

// 右モータ用PID制御 /////////////////////////////////////////////
float PID_LIGHT_R(short Senser_Value, short Target_Value){
  static long diff_R[2]={0};//偏差[0]=前回、[1]=今回
  static float integral_R=0;//積分の値（合計）
  float p=0,i=0,d=0;//今回の各値

  diff_R[0] = diff_R[1];//偏差の前回の値更新
  diff_R[1] = Target_Value - Senser_Value;//目標との偏差(今回)を計算
  if( (diff_R[1] > 100) || (diff_R[1] < -100) ){
    //処理なし(1時停止)
  }
  else{
    integral_R += (diff_R[1] + diff_R[0]) * DELTA_T / 2;//積分がグラフの面積(台形の総計)で求める
  }
  integral_R += (diff_R[1] + diff_R[0]) * DELTA_T / 2;//積分がグラフの面積(台形の総計)で求める
  p = KP * diff_R[1];//P項目を計算
  i = KI * integral_R;//I項目を計算
  d = KD * (diff_R[1] - diff_R[0]) / DELTA_T;//p項目は偏差の差で求める
  snprintf(lcd_buff2, 16, "%3.0f, %3.0f, %3.0f", p, i, d);

  return math_limit(p+i+d);
}

